.. _swh-graph:

.. include:: README.rst


.. toctree::
   :maxdepth: 1
   :caption: Overview

   quickstart
   api
   grpc-api
   java-api
   memory
   compression
   luigi
   cli
   docker
   git2graph
   example-dataset

.. only:: standalone_package_doc

   Indices and tables
   ------------------

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
